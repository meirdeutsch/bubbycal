import "./App.scss";
import AllDays from "./components/AllDays";
import PageNextYear from "./components/PageNextYear";

function App() {
	return (
		<>
			{/* <div className="monkeyface">
				<a href="#AllDays">AllDays</a>
				<a href="#PageNextYear">PageNextYear</a>
			</div> */}
			{/* <PageNextYear /> */}
			<AllDays />
		</>
	);
}
export default App;

import * as Cal from "./CalFunctions";

const MonthlySpread = () => {
	return (
		<main id="AllDays">
			{Cal.monthsOfYear.map((month, idx) => (
				<section key={idx}>
					<div className="monthBox" data-key={month.name} data-firstdate={month.firstPageDate}>
						<div className="row bg-black text-center text-white fw-bold">
							{Cal.daysOfWeek.map((day, idx) => (
								<div key={idx} className="col-1">
									{day}
								</div>
							))}
						</div>
						<div className="row align-items-stretch flex-fill position-relative">
							{Object.keys(Cal.perfect).map((key) => {
								const keyDate = new Date(key);
								const startDate = month.firstPageDate;
								const endDate = month.lastPageDate;
								const inMonth = (idx === keyDate.getMonth()).toString();
								return (
									keyDate >= startDate &&
									keyDate <= endDate && (
										<div className="calendarBox" data-fulldate={keyDate} key={key} data-inmonth={key} data-inmonth={inMonth}>
											<div data-infotype="englishDate">{keyDate.getDate().toString()}</div>
											{Cal.perfect[key].map((day, idx) => (
												<div key={idx} data-infotype={day.Location} data-infovalue={day.Subject}>
													{day.Location === "Hebrew Date" ? (
														Cal.hebrewDateSlicer(day.Subject)
													) : day.Location === "Birthday" ? (
														<div className="birthday">
															<span>
																{day.Subject}
																<small>{day.Language === "Hebrew" ? "Heb Birthday" : "Eng Birthday"}</small>
															</span>
															<img alt={day.Subject} src={day.Photo} />
														</div>
													) : day.Location === "Anniversary" ? (
														<div className="anniversary">
															<span>
																{day.Subject}
																<small>{day.Language === "Hebrew" ? "Heb Anniversary" : "Eng Anniversary"}</small>
															</span>
															<img alt={day.Subject} src={day.Photo} />
														</div>
													) : day.Subject === "Candle lighting" ? (
														<span>Candles: {day.Time}</span>
													) : day.Subject === "Havdalah" ? (
														<span>Havdala: {day.Time}</span>
													) : day.Subject === "Fast ends" ? (
														<span>Fast ends: {day.Time}</span>
													) : day.Subject === "Fast begins" ? (
														<span>Fast begins: {day.Time}</span>
													) : (
														day.Subject
													)}
												</div>
											))}
											<div className="notes"></div>
										</div>
									)
								);
							})}
						</div>
					</div>
					<div className="sidebar">
						<div className="monthName">
							{month.name}
							<br />
							{month.hebrewName}
						</div>
						<div className="notes"></div>
					</div>
				</section>
			))}
		</main>
	);
};
export default MonthlySpread;

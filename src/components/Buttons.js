import Button from "react-bootstrap/Button";

const Buttons = () => {
	return (
		<div className="d-flex gap-3 m-3 p-3">
			<Button variant="primary">Primary</Button>
			<Button variant="secondary">Secondary</Button>
			<Button variant="success">Success</Button>
			<Button variant="info">Info</Button>
			<Button variant="warning ">Warning</Button>
			<Button variant="danger">Danger</Button>
			<Button variant="light">Light</Button>
			<Button variant="dark">Dark</Button>
		</div>
	);
};
export default Buttons;

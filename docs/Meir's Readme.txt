8888888b.                                88888888888 888                    8888888b.           888             
888   Y88b                                   888     888                    888  "Y88b          888             
888    888                                   888     888                    888    888          888             
888   d88P 888d888 .d88b.  88888b.           888     88888b.   .d88b.       888    888  8888b.  888888  8888b.  
8888888P"  888P"  d8P  Y8b 888 "88b          888     888 "88b d8P  Y8b      888    888     "88b 888        "88b 
888        888    88888888 888  888          888     888  888 88888888      888    888 .d888888 888    .d888888 
888        888    Y8b.     888 d88P          888     888  888 Y8b.          888  .d88P 888  888 Y88b.  888  888 
888        888     "Y8888  88888P"           888     888  888  "Y8888       8888888P"  "Y888888  "Y888 "Y888888 
                           888                                                                                  
                           888                                                                                  
                           888                                                                                  

1.	Go to HebCal, at the following URL, replacing the 2022 with the year that you want:
	https://www.hebcal.com/hebcal?v=1&maj=on&min=on&nx=on&mf=on&ss=on&mod=on&o=on&s=on&i=on&year=2022&yt=G&lg=s&d=on&c=off&geo=none

2.	Hit the DOWNLOAD button, choose calendar app = CSV, and download the USA flavor.

3.	Convert that CSV file into JavaScript-friendly JSON at: https://www.convertcsv.com/csv-to-json.htm
	..."Choose file"
	..."Download result"

4:	Find/Change the labels in the JSON to match what the app is expecting (including dropping the quote marks):
	"Subject"	 > Subject
	"Start Date" > Date
	"Start Time" > Time
	"Location"   > Location



 .d8888b.                            888           8888888b.           888                     
d88P  Y88b                           888           888  "Y88b          888                     
Y88b.                                888           888    888          888                     
 "Y888b.    .d8888b 888d888 888  888 88888b.       888    888  8888b.  888888 .d88b.  .d8888b  
    "Y88b. d88P"    888P"   888  888 888 "88b      888    888     "88b 888   d8P  Y8b 88K      
      "888 888      888     888  888 888  888      888    888 .d888888 888   88888888 "Y8888b. 
Y88b  d88P Y88b.    888     Y88b 888 888 d88P      888  .d88P 888  888 Y88b. Y8b.          X88 
 "Y8888P"   "Y8888P 888      "Y88888 88888P"       8888888P"  "Y888888  "Y888 "Y8888   88888P' 
                                                                                               
                                                                                               
1. find any rosh chodesh items in the date array and remove " 57**" from the subject, otherwise birthdays won't appear
2a. if it's not a hebrew leap year, make shure hadassah kehati's birthday is set to Adar and not Adar II
2b. if it's  a hebrew leap year, make shure hadassah kehati's birthday is set to Adar I and not Adar II
